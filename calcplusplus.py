#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Ignacio Cruz de la Haza
import sys
import calcoohija
import csv


if __name__ == '__main__':

    if len(sys.argv) != 2:
        sys.exit('Usage: python3 calcplus.py fichero')

    with open(sys.argv[1]) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            operador = row[0]

            try:
                operando1 = float(row[1])
                operando2 = float(row[2])
            except ValueError:
                result = 'No se ha introducido un numero'
                print(result)
                continue

            c = calcoohija.CalculadoraHija(operando1, operador, operando2)
            result = c.operar()
            for operandos in row[3:]:
                try:
                    c.operando1 = float(result)
                    c.operando2 = float(operandos)
                    result = c.operar()
                except ValueError:
                    result = 'Division by zero is not allowed'

            if not result:
                result = 'Solo se puede sumar, restar, multiplicar y dividir'
            print(result)
