#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Ignacio Cruz de la Haza
import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multiplica(self):
        """
        """
        return self.operando1 * self.operando2

    def divide(self):
        """
        """
        try:
            return self.operando1 / self.operando2
        except ZeroDivisionError:
            return "Division by zero is not allowed"
        except ValueError:
            return "Division by zero is not allowed"

    def operar(self):
        result = calcoo.Calculadora.operar(self)
        if result is None:
            if self.operador == 'multiplica':
                result = self.multiplica()
            elif self.operador == 'divide':
                result = self.divide()
        return result


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("Usage: python3 calcoohija.py operando1 operador operando2")
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    operador = sys.argv[2]
    objeto = CalculadoraHija(operando1, operador, operando2)
    result = objeto.operar()
    if not result:
        result = 'Solo se puede sumar, restar, multiplicar y dividir'
    print(result)
