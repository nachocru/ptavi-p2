#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Ignacio Cruz de la Haza
import sys


class Calculadora():
    """
    Mi clase calculadora
    """
    def __init__(self, operando1, operador, operando2):
        """
        Mi constructor
        """
        self.operando1 = operando1
        self.operador = operador
        self.operando2 = operando2

    def suma(self):
        """
        """
        return self.operando1 + self.operando2

    def resta(self):
        """
        """
        return self.operando1 - self.operando2

    def operar(self):
        if self.operador == 'suma':
            return self.suma()
        elif self.operador == 'resta':
            return self.resta()
        else:
            return None


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit("Usage: python3 calcoo.py operando1 operador operando2")
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    operador = sys.argv[2]
    objeto = Calculadora(operando1, operador, operando2)
    result = Calculadora.operar(objeto)
    if not result:
        result = 'Solo se puede sumar y restar'
    print(result)
