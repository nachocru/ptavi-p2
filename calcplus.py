#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Ignacio Cruz de la Haza
import sys
import calcoohija


if __name__ == '__main__':

    if len(sys.argv) != 2:
        sys.exit('Usage: python3 calcplus.py fichero')

    fich = open(sys.argv[1], 'r')
    lineas = fich.readlines()
    for linea in lineas:
        if linea[-1] == '\n':
            palabras = linea[:-1].split(',')
        else:
            palabras = linea.split(',')
        operador = palabras[0]
        try:
            operando1 = float(palabras[1])
            operando2 = float(palabras[2])
        except ValueError:
            result = 'No se ha introducido un numero'
            print(result)
            continue
        c = calcoohija.CalculadoraHija(operando1, operador, operando2)
        result = c.operar()
        for operandos in palabras[3:]:
            try:
                c.operando1 = float(result)
                c.operando2 = float(operandos)
                result = c.operar()
            except ValueError:
                result = 'Division by zero is not allowed'

        if not result:
            result = 'Solo se puede sumar, restar, multiplicar y dividir'
        print(result)
    fich.close()
